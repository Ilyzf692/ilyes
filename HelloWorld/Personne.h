#pragma once
#include <string>

class Personne
{
private:
	std::string nom;
	int age;

public:
	Personne();
	Personne(const std::string& nom, const int& age);
	virtual ~Personne();

	std::string GetNom() { return nom; }
	int GetAge();
};

