#pragma once
#include <string>

class Feutre {
private:
    std::string couleur;
    bool bouche;

public:
    // Constructeur
    Feutre(std::string couleur);

    // Destructeur
    ~Feutre();

    // Getter pour la couleur
    std::string GetCouleur() const;

    // Getter pour la bouche
    bool EstBouche() const;

    // M�thode pour �crire un message
    void EcrireMessage(const std::string& message);

    // M�thode pour boucher le feutre
    void Bouche();

    // M�thode pour d�boucher le feutre
    void Debouche();
};
