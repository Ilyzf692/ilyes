#include "Feutre.h"
#include <iostream>

// Constructeur
Feutre::Feutre(std::string couleur) : couleur(couleur), bouche(false) {}

// Destructeur
Feutre::~Feutre() {}

// Getter pour la couleur
std::string Feutre::GetCouleur() const {
    return couleur;
}

// Getter pour la bouche
bool Feutre::EstBouche() const {
    return bouche;
}

// M�thode pour �crire un message
void Feutre::EcrireMessage(const std::string& message) {
    if (!bouche) {
        std::cout << red << "Couleur du feutre : " << red << couleur << "\nMessage : " << reset << message << std::endl;
    }
    else {
        std::cout << "Le feutre est bouche. Debouchez-le pour ecrire un message." << std::endl;
    }
}

// M�thode pour boucher le feutre
void Feutre::Bouche() {
    bouche = true;
    std::cout << "Feutre bouche." << std::endl;
}

// M�thode pour d�boucher le feutre
void Feutre::Debouche() {
    bouche = false;
    std::cout << "Feutre debouche." << std::endl;
}
