#include <iostream>
#include <iomanip>
#include "mesFonctions.h"
#include "Personne.h"
#include "Feutre.h"  // Ajout de l'inclusion pour la classe Feutre

using namespace std;

// Les couleurs en ANSI
const std::string reset("\033[0m");
const std::string red("\033[0;31m");
const std::string green("\033[0;32m");
const std::string yellow("\033[0;33m");
const std::string blue("\033[0;34m");
const std::string magenta("\033[0;35m");
const std::string cyan("\033[0;36m");

int main()
{
    cout << "Hello World!\n";

    char rep;
    rep = 0x4e; // code ascii de 'N', équivalent à rep = 'N';
    cout << "Rep: " << rep << endl;
    cout << "Rep: 0x" << std::setfill('0')
        << std::setw(4)
        << std::hex
        << static_cast<int>(rep);

    cout << dec << endl;
    int i = 10;
    cout << "uneFonction(" << i << ") = " << uneFonction(i) << endl;

    double d = 10.5;
    cout << "uneFonction(" << d << ") = " << uneFonction(d) << endl;

    cout << endl << "Creation p1 automatique." << endl;
    Personne p1("Truc", 25);
    cout << blue << "p1: " << &p1 << " " << p1.GetNom() << " a " << p1.GetAge() << " ans." << reset << endl;

    cout << endl << "Creation p2 dynamique." << endl;
    Personne* p2;
    p2 = new Personne("Machin", 35);
    cout << red << "p2: " << p2 << " " << p2->GetNom() << " a " << p2->GetAge() << " ans." << reset << endl;
    //  Libère l'espace mémoire alloué à p2
    delete p2;

    cout << endl << "Creation p3 automatique." << endl;
    Personne p3;
    cout << yellow << "p3: " << &p3 << " " << p3.GetNom() << " a " << p3.GetAge() << " ans." << reset << endl;

    // Utilisation de la classe Feutre
    cout << endl << "Utilisation de la classe Feutre." << endl;
    Feutre feutreRouge("rouge");
    Feutre feutreBleu("bleu");

    feutreRouge.Debouche();
    feutreRouge.EcrireMessage("Bonjour en rouge!");

    feutreBleu.Bouche();
    feutreBleu.EcrireMessage("Erreur en bleu!");

    cout << endl << endl << "***  FIN  ***" << endl;

    return 0;
}
