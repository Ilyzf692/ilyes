#include "mesFonctions.h"
#include <iostream>

using namespace std;


int uneFonction(int x)
{
    cout << "Une fonction avec un parametre entier " << x << endl;
    return (2 * x);
}

double uneFonction(double x)
{
    cout << "Surcharge de la fonction \"uneFonction\" avec un parametre reel " << x << endl;
    return 2.0 * x;
}
