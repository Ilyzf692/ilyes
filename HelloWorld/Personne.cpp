#include <iostream>
#include "Personne.h"


Personne::Personne() : Personne("anonyme", 0)
{
//	this->age = 0;
#ifdef _DEBUG
	std::cout << "--> Personne() de " << this << std::endl;
#endif
}

Personne::Personne(const std::string& nom, const int& age) : nom(nom)
{
	this->age = age;
#ifdef _DEBUG
	std::cout << "--> Personne(" << nom << ", " << age << ") de " << this << std::endl;
#endif
}

Personne::~Personne()
{
#ifdef _DEBUG
	std::cout << "--> ~Personne() de " << this << std::endl;
#endif
}

int Personne::GetAge()
{
	return age;
}
